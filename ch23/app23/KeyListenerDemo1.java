package app23;
import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class KeyListenerDemo1 extends JFrame 
        implements KeyListener {
    public KeyListenerDemo1(String title) {
        super(title);
        this.getContentPane().setLayout(new BorderLayout());
        JTextField textField = new JTextField(20);
        textField.addKeyListener(this);
        this.getContentPane().add(textField);
    }

    public void keyTyped(KeyEvent e) {
        e.setKeyChar(Character.toUpperCase(e.getKeyChar()));
    }

    public void keyPressed(KeyEvent e) {
    }

    public void keyReleased(KeyEvent e) {
    }

    private static void constructGUI() {
        // Make sure we have nice window decorations.
        JFrame.setDefaultLookAndFeelDecorated(true);
        KeyListenerDemo1 frame = 
                new KeyListenerDemo1("KeyListener Demo 1");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                constructGUI();
            }
        });
    }
}