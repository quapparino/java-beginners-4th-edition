package app23;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class ActionListenerDemo3 extends JFrame {
    String fileSelected;

    public ActionListenerDemo3(String title) {
        super(title);
    }

    public void init() {
        JButton button = new JButton("Select File");
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                int returnVal = fileChooser.showOpenDialog(null);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    fileSelected = 
                           fileChooser.getSelectedFile().getName();
                    System.out.print(fileSelected);
                }
            }
        });
        this.getContentPane().add(button);
    }

    private static void constructGUI() {
        JFrame.setDefaultLookAndFeelDecorated(true);
        ActionListenerDemo3 frame = new ActionListenerDemo3(
                "ActionListener Demo 3");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.init();
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                constructGUI();
            }
        });
    }
}