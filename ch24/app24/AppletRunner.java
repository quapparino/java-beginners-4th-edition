package app24;
import java.applet.Applet;
import java.awt.BorderLayout;
import javax.swing.JFrame;

public class AppletRunner extends JFrame {
    private static final long serialVersionUID = 
            -4158064205501217649L;

    public void run(String appletClassName) {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new BorderLayout());
        this.setTitle("Applet Runner");
        Applet applet = null;
        try {
            // use reflection to instantiate the applet
            Class appletClass = Class.forName(appletClassName);
            applet = (Applet) appletClass.newInstance();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        if (applet!=null) {
            this.add(applet);
            this.pack();
            this.setVisible(true);
            // call the applet's lifecycle methods
            applet.init();
            applet.start();
        }
        else {
            System.exit(-1);
        }
    }

    public static void main(String[] args) {
        if (args.length!=1) {
            System.out.println(
                    "Usage: AppletRunner appletClassName");
            System.exit(0);
        }
        // args[0] should be the fully qualified class name of the
        // applet to be run
        (new AppletRunner()).run(args[0]);
    }
}