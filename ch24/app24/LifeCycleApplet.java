package app24;
import java.applet.Applet;
import java.awt.Graphics;

public class LifeCycleApplet extends Applet {
    StringBuilder stringBuilder = new StringBuilder();
    public void init() {
        stringBuilder.append("init()... ");
    }
    public void start() {
        stringBuilder.append("start()... ");
    }
    public void stop() {
        stringBuilder.append("stop()... ");
    }
    public void destroy() {
        stringBuilder.append("destroy()... ");
    }
    public void paint(Graphics g) {
        stringBuilder.append("paint(g)... ");
        g.drawString(stringBuilder.toString(), 4, 10);
    }
}